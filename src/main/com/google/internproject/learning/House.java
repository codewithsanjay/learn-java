package com.google.internproject.learning;

public class House {

    private String description;
    private int numberOfRooms;
    private int numberOfWindows;
    private String color;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public int getNumberOfWindows() {
        return numberOfWindows;
    }

    public void setNumberOfWindows(int numberOfWindows) {
        this.numberOfWindows = numberOfWindows;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void talk(){
        System.out.println("Hello. I am house with: " + description);
        System.out.println("The color of the house is: " + color);
    }

    public void tellMeHowManyRooms(){
        System.out.println("I have " + numberOfRooms + " in this house");
    }
}
