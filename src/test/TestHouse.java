import com.google.internproject.learning.House;
import org.junit.Test;

public class TestHouse {

    @Test
    public void test1(){
        House house = new House();
        house.setDescription("Bunglow with 2 acres of land");
        house.setNumberOfRooms(5);
        house.setColor("Gray");

        house.talk();
        house.tellMeHowManyRooms();

        System.out.println("---- ");
        house.setColor("White with red stripes");
        house.talk();
        house.tellMeHowManyRooms();

    }
}
